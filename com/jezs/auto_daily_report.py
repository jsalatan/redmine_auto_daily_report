'''
Created on Feb 3, 2013

@author: Jonathan
'''
import redmine_auth
import mailer
import report

if __name__ == '__main__':
  # Create a redmine connection
  cookie = redmine_auth.connect_using_httplib2_cookies()
  
  # Pass cookies/session to report module for report generation
  final_report = report.get_final_report(cookie)
#  final_report = report.get_dummy_tracker_report()
  
  # Pass report as email body
  mailer.send_mail(final_report)