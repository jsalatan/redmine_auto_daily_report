'''
Created on Feb 3, 2013

@author: Jonathan
'''
from bs4 import BeautifulSoup
import report_parser
import httplib2
import premailer

project_url = "https://redmine.dev.novare.com.hk/redmine/projects/psb-aas-v61/"
report_uri = "issues/report/"
tracker_uri = "tracker/"
priority_uri = "priority/"
roadmap_uri = "roadmap/?completed=1"

'''
1. Get HTML content
2. Parse HTML content to extract only information needed for email
3. Apply style
4. Return report
'''
def get_final_report(cookie):
  # Get HTML content
  roadmap_html = get_html("raodmap", cookie)
  tracker_html = get_html("tracker", cookie)
  priority_html = get_html("priority", cookie)

  # Extract report tables
  module_report_table = report_parser.generate_percent_done_table(roadmap_html)
  tracker_report_table = report_parser.parse_table(tracker_html)
  priority_report_table = report_parser.parse_table(priority_html)
  
  final_report = generate_report(module_report_table, tracker_report_table, priority_report_table)
  return final_report

def get_html(report_type, cookie):
  tmp_uri = ""
  if (report_type == "tracker"):
    tmp_uri = report_uri + tracker_uri
  elif (report_type == "priority"):
    tmp_uri = report_uri + priority_uri
  else:
    tmp_uri = roadmap_uri
  
  report_url = project_url + tmp_uri
  http = httplib2.Http(disable_ssl_certificate_validation=True)
  headers = {'Cookie': cookie}
  response, content = http.request(report_url, 'GET', headers=headers)
  return content

def generate_report(module_report, tracker_report, priority_report):
  html_report = """
  <html>
  <head>
  <style type='text/css'>
    table.list { border: 1px solid #e4e4e4;  border-collapse: collapse; width: 100%%; margin-bottom: 4px; }
    table.list th {  background-color:#EEEEEE; padding: 4px; white-space:nowrap; }
    table.list td { vertical-align: top; }
    table.list td.id { width: 2%%; text-align: center;}
    table.list caption { text-align: left; padding: 0.5em 0.5em 0.5em 0; }
    table.list tbody tr:hover { background-color:#ffffdd; }
    table.list tbody tr.group:hover { background-color:inherit; }
    table td {padding:2px;}
    table p {margin:0;}
    .odd {background-color:#f6f7f8;}
    .even {background-color: #fff;}
    table.attributes { width: 100%% }
    table.attributes th { vertical-align: top; text-align: left; }
    table.attributes td { vertical-align: top; }
  </style>
  </head>
  <body>
    <div>
      <p>Good day,<p>
      <p>Please see tables below for the roadmap, tracker, and priority reports from Redmine.<p>
    </div>
    <h3>Roadmap</h3>
    %s
    <h3>Trackers</h3>
    %s
    <h3>Priority</h3>
    %s
    <div>
      <br/>
      <p>Best Regards,</p>
      <p>-- Jonathan</p>
    </div>
  </body>
  </html>""" % (module_report, tracker_report, priority_report)

  return premailer.transform(html_report)

def get_dummy_tracker_report():
  tracker_html = open('C:\\Users\\Jonathan\\Desktop\\redmine_report\\redmine_priority2.html').read()

  # Extract report tables
  tracker_report_table = report_parser.parse_table(tracker_html)
  final_report = generate_report(tracker_report_table, "priority_report_table")
  return final_report

def pre_mailer(final_report):
#  http = httplib2.Http(disable_ssl_certificate_validation=True)
#  headers = {'Cookie': cookie}
  soup = BeautifulSoup(final_report)
  stylesheets = soup.findAll("link", {"rel": "stylesheet"})
  for s in stylesheets:
#    remote_css = http.get
    s.replaceWith('<style type="text/css" media="screen">' + open(s["href"]).read() + '</style>')
  return str(soup)

if __name__ == '__main__':
  print (get_dummy_tracker_report())