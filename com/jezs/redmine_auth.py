'''
Created on Feb 3, 2013

@author: Jonathan
'''
from httplib import HTTPSConnection, HTTPConnection
import base64
import httplib2
import requests
import urllib
import urllib2
import logging

logger = logging.getLogger('redmine_auth')
redmine_url = "https://redmine.dev.novare.com.hk/redmine/projects/psb-aas-v61/"
report_url = "https://redmine.dev.novare.com.hk/redmine/projects/psb-aas-v61/issues/report/tracker"
login_url = "https://redmine.dev.novare.com.hk/redmine/login"

def connect_using_urllib2():
#  conn = HTTPSConnection('10.10.10.10','443', cert_file =CERT_FILE)
  
  password_manager = urllib2.HTTPPasswordMgrWithDefaultRealm()
  password_manager.add_password(None, login_url, 'jonathan.salatan', 'P3@chpuff!')
  
  handler = urllib2.HTTPBasicAuthHandler(password_manager)
  opener = urllib2.build_opener(handler)
  opener.open(login_url)
   
  urllib2.install_opener(opener)
  
  credentials = {'username':'jonathan.salatan', 'password':'P3@chpuff!'}
  payload = urllib.urlencode(credentials)
  
  req = urllib2.Request(login_url, data=payload)
  handler = urllib2.urlopen(req)
   
  print handler.getcode()
  print handler.headers.getheader('content-type')
  print handler.read()
  
  return opener

def connect_using_requests():
  credentials = {'username':'jonathan.salatan', 'password':'P3@chpuff!'}
  payload = urllib.urlencode(credentials)
  
  response = requests.get(login_url, data=payload, verify=False)
  
#  r = requests.get(redmine_url, data=payload, verify=False)
  response = requests.post("https://redmine.dev.novare.com.hk/redmine/login",data={'username':'jonathan.salatan', 'password':'P3@chpuff!'})
  print(response.text)

def connect_using_httplib2():
  h = httplib2.Http(".cache")
  h.add_credentials('jonathan.salatan', 'P3@chpuff!')
  
  resp, content = h.request(login_url, "PUT", body="This is text", headers={'content-type':'text/plain'} )
  print (resp)
  print (content)

class RedmineConnection:
  def __init__(self):
    self.__url = login_url  
    self.__body = {'username': 'jonathan.salatan', 'password': 'P3@chpuff!'}
    self.__headers = {'Content-type': 'application/x-www-form-urlencoded'}
  
def connect_using_httplib2_cookies():
  url = login_url  
  body = {'username': 'jonathan.salatan', 'password': 'P3@chpuff!'}
  headers = {'Content-type': 'application/x-www-form-urlencoded'}

  http = httplib2.Http(disable_ssl_certificate_validation=True)
  response, content = http.request(url, 'POST', headers=headers, body=urllib.urlencode(body))

  headers = {'Cookie': response['set-cookie']}
  response, content = http.request(report_url, 'GET', headers=headers)

  return response['set-cookie']
  

if __name__ == '__main__':
  HTTPConnection.debuglevel = 1
#  connect_using_urllib2()
#  connect_using_requests()
#  connect_using_httplib2()
  connect_using_httplib2_cookies()
#  report_url = "https://redmine.dev.novare.com.hk/redmine/projects/psb-aas-v61/issues/report/priority"

#  response = urllib2.urlopen(report_url)
#  print response.read()  
