'''
Created on Feb 3, 2013

@author: Jonathan
'''
from bs4 import BeautifulSoup
from bs4.element import NavigableString
class ReportParser:
  def __init__(self):
    pass

def parse_table(html):
  soup = strip_tags(html, ['a'])
  table = soup.table
  return table

def strip_tags(html, invalid_tags):
    soup = BeautifulSoup(html)
    for tag in soup.findAll(True):
        if tag.name in invalid_tags:
            s = ""
            for c in tag.contents:
                if not isinstance(c, NavigableString):
                    c = strip_tags(unicode(c), invalid_tags)
                s += unicode(c)
            tag.replaceWith(s)
    return soup

module_list = ["Request Receiving", "Request Processing", "Request Assignment", 
               "Request Fulfillment (Backend)", "Request Fulfillment (Desktop)",
               "Request Fulfillment (iPad)", "Report Generation", "Underlying Functions",
               "Missed tasks"]

'''
1. Find target module
2. Find module % done
'''
def generate_percent_done_table(html):
  percent_dict = parse_percent_done(html)
  percent_rows = generate_percent_rows(percent_dict)
  percent_tbl = """
  <table style="border-collapse:collapse; margin-bottom:4px;
   border:1px solid #e4e4e4">
    <thead>
      <th style="padding:4px; background-color:#EEEEEE;
       white-space:nowrap" bgcolor="#EEEEEE">Module</th>
      <th style="padding:4px; background-color:#EEEEEE;
       white-space:nowrap" bgcolor="#EEEEEE">%% Done</th>
      <th style="padding:4px; background-color:#EEEEEE;
       white-space:nowrap" bgcolor="#EEEEEE">Open</th>
      <th style="padding:4px; background-color:#EEEEEE;
       white-space:nowrap" bgcolor="#EEEEEE">Closed</th>
      <th style="padding:4px; background-color:#EEEEEE;
       white-space:nowrap" bgcolor="#EEEEEE">Total</th>
    </thead>
    %s
  </table>""" % percent_rows
  return percent_tbl

style_odd = 'style="{background-color:#f6f7f8} :hover{background-color:#ffffdd}" bgcolor="#f6f7f8"'
style_even = 'style="{background-color:#fff} :hover{background-color:#ffffdd}" bgcolor="#fff"'

def generate_percent_rows(percent_dict):
  row = """
  <tr %s>
    <td style="padding:2px; vertical-align:top">%s</td>
    <td align="center" style="padding:2px; vertical-align:top">%s</td>
    <td align="center" style="padding:2px; vertical-align:top">%s</td>
    <td align="center" style="padding:2px; vertical-align:top">%s</td>
    <td align="center" style="padding:2px; vertical-align:top">%s</td>
  </tr>"""
  percent_rows = ""
  for index, key in enumerate(module_list):
    style = style_even if index % 2 == 0 else style_odd
    values = percent_dict[key] 
    percent_rows = percent_rows + row % (style, key, values[0], values[1], values[2], values[3])

  # Append Total (% Average) row
  totalRow = """
  <tr %s>
    <td style="padding:2px; vertical-align:top; font-weight:bold">%s</td>
    <td align="center" style="padding:2px; vertical-align:top; font-weight:bold">%s</td>
    <td align="center" style="padding:2px; vertical-align:top; font-weight:bold">%s</td>
    <td align="center" style="padding:2px; vertical-align:top; font-weight:bold">%s</td>
    <td align="center" style="padding:2px; vertical-align:top; font-weight:bold">%s</td>
  </tr>"""
  style = style_even if len(module_list) % 2 == 0 else style_odd 
  key = "Total (% Average)"
  values = percent_dict[key]
  percent_rows = percent_rows + totalRow % (style, key, values[0], values[1], values[2], values[3])
  
  return percent_rows

def parse_percent_done(html):
  # Target module key string:
  '''
  <h3 class="version"><a name="Request_Fulfillment_(Backend)" /><a href="/redmine/versions/305">Request Fulfillment (Backend)</a></h3>
  '''
  # % Done key string:
  '''
  <p class="pourcent">93%</p>
  '''
  percent_done_dict = {}
  st_idx = html.find('<h3 class="version">')

  # percent array holder to compute for total (% average)
  total_open = 0
  total_closed = 0
  total_issues = 0

  for module in module_list:
    module_idx = html.find(module.replace(" ", "_"), st_idx)
    
    # initialize array for values [%Done, Open, Closed, Total]
    values = [None]*4

    # %Done
    st_key = '<p class="pourcent">'
    percent_st = html.find(st_key, module_idx) + len(st_key)
    percent_en = html.find('</p>', percent_st)
    
    percentStr = html[percent_st:percent_en]
    values[0] = percentStr

    # Total
    dummy_key = '<p class="progress-info">'
    dummy_st = html.find(dummy_key, percent_en) + len(dummy_key)
    st_key = '">'
    total_st = html.find(st_key, dummy_st) + len(st_key)
    total_en = html.find(' issues</a>', total_st)

    totalStr = html[total_st:total_en]
    values[3] = totalStr

    # Closed
    st_key = '">'
    closed_st = html.find(st_key, total_en) + len(st_key)
    closed_en = html.find(' closed</a>', closed_st)

    closedStr = html[closed_st:closed_en]
    values[2] = closedStr
    
    # Open
    if totalStr != closedStr:
      st_key = '">'
    else:
      st_key = "&#8212;"
    open_st = html.find(st_key, closed_en + len(' closed</a>')) + len(st_key)
    open_en = html.find(' open', open_st)

    openStr = html[open_st:open_en]
    values[1] = openStr
    
    percent_done_dict[module] = values

    # sum all open, closed, and issues
    total_open = total_open + int(openStr)
    total_closed = total_closed + int(closedStr)
    total_issues = total_issues + int(totalStr)

  # [%Done, Open, Closed, Total]
  percent = (float(total_closed) / total_issues) * 100
  done = "%.2f" % percent
  totalValues = [ done + "%", str(total_open), str(total_closed), str(total_issues)]
  percent_done_dict["Total (% Average)"] = totalValues

  opStr = "Done: %s / Open: %s / Closed: %s / Total: %s"
  print( opStr % (totalValues[0], totalValues[1], totalValues[2], totalValues[3]))

  return percent_done_dict

if __name__ == '__main__':
#   report = open('C:\\Users\\Jonathan\\Desktop\\redmine_report\\redmine_priority2.html')
#   print (parse_table(report.read()))
  report = open('C:\\Users\\Jonathan\\Desktop\\redmine_report\\roadmap.html')
  print (generate_percent_done_table(report.read()))