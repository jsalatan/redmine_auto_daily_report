'''
Created on Feb 3, 2013

@author: Jonathan
'''
# Import smtplib for the actual sending function
from email.mime.text import MIMEText
from smtplib import SMTP_SSL
import datetime

# Import the email modules we'll need

me = "Jonathan Salatan <jonathan.salatan@novare.com.hk>"
# TODO: Edit/add recepients
to_list = ["jonathan.salatan@novare.com.hk"]

def send_mail(final_report):
  msg = MIMEText(final_report, 'html')
  
  # me == the sender's email address
  # you == the recipient's email address
  subj_date = datetime.date.today().strftime( "%b. %d, %Y" )
  msg['Subject'] = '[PSBank AAS] Daily Redmine Reports - %s' % subj_date
  msg['From'] = me
  msg['To'] = ",".join(to_list)
  msg['Body'] = final_report
  
  # Send the message via our own SMTP server, but don't include the
  # envelope header.
  s = SMTP_SSL('mails.novare.com.hk', 465)
  s.login("jonathan.salatan@novare.com.hk", "P3@chpuff!")
  s.set_debuglevel(1)
  s.sendmail(me, ",".join(to_list), msg.as_string())
  s.quit()
  
if __name__ == '__main__':
#  final_report = "test"
  report = open('C:\\Users\\Jonathan\\Desktop\\redmine_report\\redmine_priority2.html')
  final_report = report.read()
  send_mail(final_report)